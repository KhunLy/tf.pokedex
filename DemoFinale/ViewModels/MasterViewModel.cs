﻿using DemoFinale.Models;
using Newtonsoft.Json;
using System.Collections.ObjectModel;
using System.Net.Http;
using ToolBox.MVVM.Base;
using ToolBox.MVVM.Commands;
using ToolBox.MVVM.Mediator;

namespace DemoFinale.ViewModels
{
    class MasterViewModel : BindableBase
    {
        private ObservableCollection<Result> results;

        public ObservableCollection<Result> Results
        {
            get { return results; }
            set { results = value; OnPropertyChanged(); }
        }

        private Result selectedPokemon;

        public Result SelectedPokemon
        {
            get { return selectedPokemon; }
            set { 
                selectedPokemon = value;
                if(selectedPokemon != null)
                {
                    Messenger<string>.Publish("P_CHANGED", selectedPokemon.Url);
                }
            }
        }


        public string Prev { get; set; }
        public string Next { get; set; }

        public CommandBase PrevCommand { get; set; }
        public CommandBase NextCommand { get; set; }

        public MasterViewModel()
        {
            //PrevCommand = new CommandBase(GoPrev, () => Prev != null);
            //NextCommand = new CommandBase(GoNext,  () => Next != null);
            PrevCommand = new CommandBase(() => Load(Prev), () => Prev != null);
            NextCommand = new CommandBase(() => Load(Next), () => Next != null);
            // rechercher les pokemons sur poke api
            Load("https://pokeapi.co/api/v2/pokemon");
        }
        //private void GoPrev() 
        //{
        //    Load(Prev);
        //}

        //private void GoNext() 
        //{
        //    Load(Next);
        //}


        private void Load(string url)
        {
            using (HttpClient client = new HttpClient())
            {
                // envoi d'une requête http + réponse
                HttpResponseMessage response = client.GetAsync(url).Result;
                if (response.IsSuccessStatusCode)
                {
                    // recupération du json
                    string json = response.Content.ReadAsStringAsync().Result;
                    // déserialisation du json (json => c#)
                    PokemonListRequest o = JsonConvert.DeserializeObject<PokemonListRequest>(json);
                    // enregistrer les pokemons dans une ObservableCollection
                    Results = new ObservableCollection<Result>(o.Results);
                    Prev = o.Previous;
                    Next = o.Next;
                    PrevCommand.OnCanExecuteChanged();
                    NextCommand.OnCanExecuteChanged();
                }
                else
                {
                    // afficher un message erreur
                }
            }
        }
    }
}
