﻿using DemoFinale.Models;
using Newtonsoft.Json;
using System;

using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using ToolBox.MVVM.Base;
using ToolBox.MVVM.Mediator;

namespace DemoFinale.ViewModels
{
    class DetailsViewModel : BindableBase
    {
        private PokemonDetailsRequest pokemon;

        public PokemonDetailsRequest Pokemon
        {
            get { return pokemon; }
            set { pokemon = value; OnPropertyChanged(); }
        }



        public DetailsViewModel()
        {
            Messenger<string>.Subscribe("P_CHANGED", LoadAsync);
        }

        private async void LoadAsync(string url)
        {
            using (HttpClient client = new HttpClient())
            {
                HttpResponseMessage response = await client.GetAsync(url);
                string json = await response.Content.ReadAsStringAsync();

                Pokemon = JsonConvert.DeserializeObject<PokemonDetailsRequest>(json);

            }
        }
    }
}
