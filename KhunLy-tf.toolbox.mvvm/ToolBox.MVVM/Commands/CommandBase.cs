﻿using System;
using System.Windows.Input;

namespace ToolBox.MVVM.Commands
{
    public class CommandBase : ICommand
    {
        public event EventHandler CanExecuteChanged;

        private Action action;
        private Func<bool> canActivate;

        public CommandBase(Action action, Func<bool> canActivate = null)
        {
            if (action is null) throw new ArgumentException();
            this.action = action;
            this.canActivate = canActivate;
        }

        public bool CanExecute(object parameter)
        {
            if(canActivate is null)
            {
                return true;
            }
            return canActivate.Invoke();
            //return canActivate?.Invoke() ?? true;
        }

        public void Execute(object parameter)
        {
            action?.Invoke();
        }

        public void OnCanExecuteChanged()
        {
            // prévient le bouton que qque chose à changer et qu'il doit mettre à jour son état
            CanExecuteChanged?.Invoke(this, null);
        }
    }
}

